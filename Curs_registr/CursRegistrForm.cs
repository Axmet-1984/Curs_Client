﻿using Curs_registr.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Curs_registr
{
    public partial class CursRegistrForm : Form
    {
        public CursRegistrForm()
        {
            InitializeComponent();
        }

        private void CursRegistrForm_Shown(object sender, EventArgs messageEx)
        {
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", 3084);

                byte[] data = new byte[2048];
                StringBuilder response = new StringBuilder();
                NetworkStream stream = client.GetStream();

                do
                {
                    int bytes = stream.Read(data, 0, data.Length);
                    
                }
                while (stream.DataAvailable);

                //десериализовать
                BinaryFormatter formatter = new BinaryFormatter();
                
                // десериализация
                using (FileStream fs = new FileStream("docs.dat", FileMode.OpenOrCreate))
                {
                    Doc_Name[] doc;



                    doc = (Doc_Name[])formatter.Deserialize(fs);

                    foreach (var p in doc)
                    {
                        Console.WriteLine("Имя: {0} --- Возраст: {1}", p.DocName, p.Id);
                    }
                }
                MessageBox.Show(response.ToString());

                stream.Close();
                client.Close();
            }
            catch (SocketException Ex)
            {
                MessageBox.Show("SocketException: {0}", Ex.ToString());
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Exception: {0}", Ex.ToString());
            }

            MessageBox.Show("Запрос завершен");
        }
    }
}
