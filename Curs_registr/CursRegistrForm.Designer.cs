﻿namespace Curs_registr
{
    partial class CursRegistrForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbDocProfile = new System.Windows.Forms.ComboBox();
            this.cbbDocName = new System.Windows.Forms.ComboBox();
            this.labDocProfile = new System.Windows.Forms.Label();
            this.LabDocName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txbPEmailAddress = new System.Windows.Forms.TextBox();
            this.txbPCityAddress = new System.Windows.Forms.TextBox();
            this.txbPPhone = new System.Windows.Forms.TextBox();
            this.txbPNmae = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbbDocProfile);
            this.groupBox1.Controls.Add(this.cbbDocName);
            this.groupBox1.Controls.Add(this.labDocProfile);
            this.groupBox1.Controls.Add(this.LabDocName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(289, 352);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Данные Врача";
            // 
            // cbbDocProfile
            // 
            this.cbbDocProfile.FormattingEnabled = true;
            this.cbbDocProfile.Location = new System.Drawing.Point(6, 122);
            this.cbbDocProfile.Name = "cbbDocProfile";
            this.cbbDocProfile.Size = new System.Drawing.Size(278, 21);
            this.cbbDocProfile.TabIndex = 3;
            // 
            // cbbDocName
            // 
            this.cbbDocName.FormattingEnabled = true;
            this.cbbDocName.Location = new System.Drawing.Point(9, 45);
            this.cbbDocName.Name = "cbbDocName";
            this.cbbDocName.Size = new System.Drawing.Size(275, 21);
            this.cbbDocName.TabIndex = 2;
            // 
            // labDocProfile
            // 
            this.labDocProfile.AutoSize = true;
            this.labDocProfile.Location = new System.Drawing.Point(6, 106);
            this.labDocProfile.Name = "labDocProfile";
            this.labDocProfile.Size = new System.Drawing.Size(86, 13);
            this.labDocProfile.TabIndex = 1;
            this.labDocProfile.Text = "Профиль Врача";
            // 
            // LabDocName
            // 
            this.LabDocName.AutoSize = true;
            this.LabDocName.Location = new System.Drawing.Point(6, 29);
            this.LabDocName.Name = "LabDocName";
            this.LabDocName.Size = new System.Drawing.Size(62, 13);
            this.LabDocName.TabIndex = 0;
            this.LabDocName.Text = "Имя Врача";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txbPEmailAddress);
            this.groupBox2.Controls.Add(this.txbPCityAddress);
            this.groupBox2.Controls.Add(this.txbPPhone);
            this.groupBox2.Controls.Add(this.txbPNmae);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(307, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(334, 352);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Данные Пациента";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(253, 323);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Выход";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(171, 323);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Очистить";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(90, 323);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Ввести";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txbPEmailAddress
            // 
            this.txbPEmailAddress.Location = new System.Drawing.Point(15, 235);
            this.txbPEmailAddress.Name = "txbPEmailAddress";
            this.txbPEmailAddress.Size = new System.Drawing.Size(285, 20);
            this.txbPEmailAddress.TabIndex = 8;
            // 
            // txbPCityAddress
            // 
            this.txbPCityAddress.Location = new System.Drawing.Point(15, 172);
            this.txbPCityAddress.Name = "txbPCityAddress";
            this.txbPCityAddress.Size = new System.Drawing.Size(285, 20);
            this.txbPCityAddress.TabIndex = 7;
            // 
            // txbPPhone
            // 
            this.txbPPhone.Location = new System.Drawing.Point(15, 106);
            this.txbPPhone.Name = "txbPPhone";
            this.txbPPhone.Size = new System.Drawing.Size(285, 20);
            this.txbPPhone.TabIndex = 6;
            // 
            // txbPNmae
            // 
            this.txbPNmae.Location = new System.Drawing.Point(15, 45);
            this.txbPNmae.Name = "txbPNmae";
            this.txbPNmae.Size = new System.Drawing.Size(285, 20);
            this.txbPNmae.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Email Адрес";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Городской Адрес";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Телефон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Полное Имя Пациента";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 0;
            // 
            // CursRegistrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 377);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CursRegistrForm";
            this.Text = "Регистратура";
            this.Shown += new System.EventHandler(this.CursRegistrForm_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbbDocProfile;
        private System.Windows.Forms.ComboBox cbbDocName;
        private System.Windows.Forms.Label labDocProfile;
        private System.Windows.Forms.Label LabDocName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbPEmailAddress;
        private System.Windows.Forms.TextBox txbPCityAddress;
        private System.Windows.Forms.TextBox txbPPhone;
        private System.Windows.Forms.TextBox txbPNmae;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}

